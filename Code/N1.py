import math
from datetime import datetime, timedelta

def string_to_timedelta(string):
    #takes a string in the format YYYY-HH-MM and produces the corosponding timedelta object
    t = datetime.strptime(string, "%Y-%m-%d-%H-%M") #comment this
    timedelta_output = timedelta(years=t.year, months=t.month, hours=t.hour, minutes=t.minute, seconds=t.second)

def range_relationship(range_haver_one, range_haver_two): 
    #returns the relationsip of range 1 to range 2, takes any block and one-time events as either parameter
    range_one_start = range_haver_one.start_time
    range_one_end = range_haver_one.end_time
    range_two_start = range_haver_two.start_time
    range_two_end = range_haver_two.end_time
   
    if range_two_start >= range_one_end: #if range one is entirely before range two
        return "before" #should these be 'leading' and 'following' as well?
    elif range_one_start >= range_two_end: #if range one is entirely after range two
        return "after"
    elif range_one_start >= range_two_start:
        return "following overlap" 
        #the entites overlap and entity 1 starts after or at the same time as entity 2, will be placed after it in the sequential list 
    elif range_one_start < range_two_start:
        return "leading overlap"

class Entry: 
    def __init__(self, input_list): #the name input_list here is technically correct but sort of misleading, as the fact that it's a list doesn't mean much  
        #these attributes will always be initilized
        self.type = input_list[0] #returns the type of entry
        self.priority = int(input_list [1]) #returns the priority of the entry as an integer
        self.name = input_list[2] #returns the name of the entry
        self.location = input_list[3] #returns the location of the entry (if any)
        self.prerequisites = input_list[4] #returns a list of prerequisite ????(blocks, entries?) of the entry (if any)
        self.external_blockers = input_list[5]
        #returns a list of external blockers of the entry (if any) 
        # (can initially be just strings, later can refer to blocks onm entries on other people's accounts,
        #  built as part of social and team coordination features)
    
        if self.type == "oe": #one-time event

            self.start_time = datetime.fromisoformat(input_list[6])
            self.end_time = datetime.fromisoformat(input_list[7])

        elif self.type == "re": #repeating event

            self.start_time = datetime.fromisoformat(input_list[6])
            self.end_time = datetime.fromisoformat(input_list[7])
            self.repetition_period = input_list[8]
            self.repetition_multiplier = input_list[9]
            self.repetition_cutoff = input_list[10]
            self.placement_time = self.start_time #???

        elif self.type == "ot": #one-time task

            self.start_time = datetime.fromisoformat(input_list[6])
            self.end_time = datetime.fromisoformat(input_list[7])
            self.max_priority = input_list[8]
            self.priority_curve = input_list[9]
            self.splittable = input_list[10]
            self.min_split = input_list[11]
            self.max_split = input_list[12]
            self.min_x_in_x = input_list[13]
            self.max_x_in_x = input_list[14]

        elif self.type == "rt": #repeating task    

            self.start_time = datetime.fromisoformat(input_list[6])
            self.end_time = datetime.fromisoformat(input_list[7])
            self.repetition_cutoff = input_list[8]
            self.length = input_list[9]
            self.deadline = input_list[10]
            self.max_priority = input_list[11]
            self.priority_curve = input_list[12]
            self.splittable = input_list[13]
            self.min_split = input_list[14]
            self.max_split = input_list[15]
            self.min_x_in_x = input_list[16]
            self.max_x_in_x = input_list[17]

        #else #??? how to throw an error here?


class Block:
    def __init__(self, Entry_object, block_count):
        self.type = Entry_object.type #returns the type of entry
        self.priority = Entry_object.priority #returns the priority of the entry as an integer
        self.name = Entry_object.name #returns the name of the entry
        self.location = Entry_object.location #returns the location of the entry (if any)
        self.prerequisites = Entry_object.prerequisites 
        #returns a list of prerequisite ????(blocks, entries?) of the entry (if any) (is this needed for all block types?)
        self.external_blockers = Entry_object.external_blockers
        #returns a list of external blockers of the entry (if any) (can initially be just strings, later can refer to blocks onm entries on other people's accounts, built as part of social and team coordination features) (is this needed for all block types?)
        self.obstructed_status = False #defaults to false, set to true after init if needed
        self.obstructing_status = False
        self.obstructors = [] #list of block numbers obsructing this one 
        self.obstructing = [] #list of block numbers which this one obsructs 
        self.number = block_count 
        #unique identifier, used for obstruction tracking and possibly prereqs and tying task blocks back to parent entries

        if self.type == "oe": #one-time event

            self.start_time = Entry_object.start_time
            self.end_time = Entry_object.end_time

        #have not worked on below this line
        elif self.type == "re": #repeating event

            self.start_time = Entry_object.start_time
            self.end_time = Entry_object.end_time
            self.repetition_period = Entry_object.repetition_period
            self.repetition_multiplier = Entry_object.repetition_multiplier
            self.repetition_cutoff = Entry_object.repetition_cutoff


        elif self.type == "ot": #one-time task

            self.length = Entry_object.length
            self.deadline = Entry_object.deadline
            self.max_priority = Entry_object.max_priority
            self.priority_curve = Entry_object.priority_curve
            self.splittable = Entry_object.splittable
            self.min_split = Entry_object.min_split
            self.max_split = Entry_object.max_split
            self.min_x_in_x = Entry_object.min_x_in_x
            self.max_x_in_x = Entry_object.max_x_in_x

        elif self.type == "rt": #repeating task    

            self.repetition_period = Entry_object.repetition_period
            self.repetition_multiplier = Entry_object.repetition_multiplier
            self.repetition_cutoff = Entry_object.repetition_cutoff
            self.length = Entry_object.length
            self.deadline = Entry_object.deadline
            self.max_priority = Entry_object.max_priority
            self.priority_curve = Entry_object.priority_curve
            self.splittable = Entry_object.splittable
            self.min_split = Entry_object.min_split
            self.max_split = Entry_object.max_split
            self.min_x_in_x = Entry_object.min_x_in_x
            self.max_x_in_x = Entry_object.max_x_in_x

        #else #??? how to throw an error here?

    def obstructor_obstructed_pair(self, obstructor, obstructed): 
        #adds the obstructor and obstructed to the other objects relevevant ID list and sets the correct flags
        obstructed.obstructed_status = True
        obstructed.obstructors.append(obstructor.number)
        obstructor.obstructing_status = True
        obstructor.obstructing.append(obstructed.number)

    def obstructed_blocks_for_printing (self, obstructing_block, block_list):
        output_string = ''
        for block in obstructing_block.obstructing:
            output_string =+ block_list[block].start_time + " - " + block_list[block].end_time + " : " +block_list[block].name + ", "
        return output_string

    def obstructing_blocks_for_printing (self, obstructed_block, block_list):
        output_string = ''
        for block in obstructed_block.obstructors:
            output_string =+ block_list[block].start_time + " - " + block_list[block].end_time + " : " +block_list[block].name + ", "
        return output_string

    def print_to_txt(self, block, txt_file, block_list):
        output_string = block.start_time + " - " + block.end_time + " : " + block.name
        if self.obstructed_status == True:
            output_string += " Blocked by: " + Block.obstructing_blocks_for_printing(block, block_list) 
        if self.obstructing_status == True:
            output_string += " Blocking: " + Block.obstructed_blocks_for_printing(block, block_list)   
        txt_file.write(output_string + '\n')  
        return                

    def place_block (self, current_block, block_list):
     
        if len(block_list) == 0: #if this is the first entry being scheduled (this is optimization) #this can be moved up a level later?
            block_list.append(current_block) #covert to a Block and move it to the blocks list

        else: 
            for placed_block in block_list:#this is where the actual placement loop   
                relationship_of_current_block_to_placed_block = range_relationship(current_block, placed_block)

                if relationship_of_current_block_to_placed_block == "before":
                    block_list.insert(placed_block, Block(current_block, block_count)) 
                    #convert the current entry into a block and insert it at the location of the current block

                elif relationship_of_current_block_to_placed_block == "overlap": 
                    #these only work if the target block is an event, need to write "overlap resolution" functions for all types inside the classes
                    if len(block_list) == placed_block: #if this is the last block which has already been placed (ugly)
                        block_list.append(current_block)
                    
                        if current_block.priority > placed_block.priotity:
                            Block.obstructor_obstructed_pair(current_block, placed_block)
                            
                        elif current_block.priority < placed_block.priotity:
                            Block.obstructor_obstructed_pair(placed_block, current_block)

                        elif current_block.priority == placed_block.priotity:
                            Block.obstructor_obstructed_pair(current_block, placed_block)
                            Block.obstructor_obstructed_pair(placed_block, current_block)

                elif relationship_of_current_block_to_placed_block == "after":
                    if len(block_list) == placed_block: #if this is the last block which has already been placed
                        block_list.append(current_block)    
        return      
   
    #def repeating_event_block_list_maker (self, repeating_event_entry, generation_length):


#initiation of the two lists

block_list = [] #declare the block list (blocks are instances of an entry placed in the timeline)
entries = [] 
#declare the entries list 
#(entries are the pieces of information representing the user's intent, they have not yet been integrated into the timeline)
generation_length = 10 #years
block_count = 0 #for making unique block identifiers

#main function, currently handles One-Time Events only

input = open("input.txt", "r") #open the input file

for line in input: #for each line of the input file
   
    current_entry = Entry(line.split('.')) #create an object of class Entry with the attributes listed in the input line
    entries.append(current_entry)  #append the current Entry class object to the entries list (may need unique id's to retrieve later)
    
    if current_entry.type == "oe": #if the current entry is a one-time event
        current_block = Block(current_entry, block_count)
        Block.place_block(current_block, block_list)

    elif current_entry.type == "re": #if the current entry is a repeating event
        blocks_placed = 0
        if generation_length > current_entry.repetition_cutoff: #does this work if repetition_cutoff is null?
            blocks_in_range = math.trunc(current_entry.repetition_cutoff / (current_entry.repetition_period * current_entry.repetition_multiplier)) -1

        else: 
            blocks_in_range = math.trunc(generation_length / (current_entry.repetition_period * current_entry.repetition_multiplier)) - 1
            #all that returns the number of blocks of the specified periodicity which can fit in the given placement time,
            #  truncated down to a whole number minus one for this first non-modfied example
            exit


        #might make this next segment a function    
        Block.place_block(Block(current_entry), block_list)
        for blocks_placed in blocks_in_range: 
            current_entry.start_time += (current_entry.repetition_period * current_entry.repetition_multiplier)
            current_entry.end_time += (current_entry.repetition_period * current_entry.repetition_multiplier)
            Block.place_block(Block(current_entry), block_list)
            #advance start and end times by the length of the specified interval and place the resulting block 
    
input.close() #close the input file

#output starts here
open("output.txt", "w") #create/wipe output file
output = open("output.txt", "a") #open ouptupt file in append mode
counter = 0

for block in block_list:
    Block.print_to_txt(block, output, block_list)

output.close() 
